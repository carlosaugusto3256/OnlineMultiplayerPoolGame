import java.util.LinkedList;
import processing.sound.SoundFile;

static class Game {
  private static LinkedList<GameObject> objects;
  private static boolean[] keys;
  private static boolean readLeftMouse, readRightMouse;
  
  public static SoundFile hit, cue, in;
  public static PVector mouseDownPosition, mouseUpPosition, mousePosition;
  public static boolean mouseLeft, mouseRight;  

  public static void tick() {
    for (int i = 0; i < objects.size(); i++)
    {
      GameObject obj = objects.get(i);
      for (int j = i+1; j < objects.size(); j++) 
      {
        boolean collide = obj.intersects(objects.get(j));

        if (collide) 
        {
          obj.tick(objects.get(j));
          //objects.get(j).tick(obj);
        }
      }

      if (!readLeftMouse) {
        if (mouseLeft && obj.contains(mouseDownPosition)) {
          obj.leftClicked = true;
          obj.leftClickDown();
          readLeftMouse = true;
        }
      } else if (!mouseLeft && obj.leftClicked) {
        obj.leftClicked = false;
        obj.leftClickUp();
        readLeftMouse = false;
      }

      if (!readRightMouse) {
        if (mouseRight && obj.contains(mouseDownPosition)) {
          obj.rightClicked = true;
          obj.rightClickDown();
          readRightMouse = true;
        }
      } else if (!mouseRight && obj.rightClicked) {
        obj.rightClicked = false;
        obj.rightClickUp();
        readRightMouse = false;
      }

      obj.tick(null);
    }
  }

  public static void init(PApplet parent) {
    mouseDownPosition = new PVector(-1, -1);
    mouseUpPosition = new PVector(-1, -1);
    mousePosition = new PVector(-1, -1);
    mouseLeft = mouseRight = false;
    keys = new boolean[300];
    objects = new LinkedList<GameObject>();
    hit = new SoundFile(parent, "hit.wav");
    cue = new SoundFile(parent, "cue.wav");
    in = new SoundFile(parent, "in.wav");
  }

  public static void render() {
    for (int i = 0; i < objects.size(); i++) {
      objects.get(i).render();
    }
  }

  public static void add(GameObject object) {
    objects.add(object);
  }

  public static void remove(GameObject object) {
    objects.remove(object);
  }

  public static boolean keyState(int key) {
    return keys[key];
  }

  public static void keyState(int key, boolean state) {
    keys[key] = state;
  }

  public static void  mouseState(float x, float y, int button, boolean state) {
    if (button == RIGHT)
      mouseRight= state;
    else
      mouseLeft = state;

    if (state) {
      mouseDownPosition = new PVector(x, y);
    } else { 
      mouseUpPosition = new PVector(x, y);
    }
  }

  public static void mousePosition(float x, float y) {
    mousePosition = new PVector(x, y);
  }
}