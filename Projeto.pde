void setup() {
  size(1062, 600);
  frameRate(60);
  background(0);

  Game.init(this);

  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 5; j++)
      Game.add(new Ball(500+(i*24), 300+(j*24), 10, 10, color(random(200) + 50, random(100)+ 50, random(100)+ 50)));

  Game.add(new Ball(200, 200, 12, 10, color(255)));
}

void draw() {
  clear();
  //table frame
  fill(180, 133, 0);
  rect(80, 80, width-160, height-160);
  fill(0, 255, 0);
  noStroke();
  rect(100, 100, width-200, height-200);

  int posx = 0, posy = 0;

  //top left hole
  stroke(204, 153, 0);
  noStroke();
  ellipse(98, 98, 30, 30);

  Game.render();
  for (int i = 0; i < 10; i++)
    Game.tick();
  fill(0, 0, 255);
  rect(mouseX-5, mouseY-5, 10, 10);
}

void keyPressed() {
  if (!Game.keyState(keyCode))
    Game.keyState(keyCode, true);
}

void keyReleased() {
  Game.keyState(keyCode, false);
}

void mousePressed() {
  Game.mouseState(mouseX, mouseY, mouseButton, true);
}

void mouseDragged() {
  Game.mousePosition(mouseX, mouseY);
}

void mouseMoved() {
  Game.mousePosition(mouseX, mouseY);
}

void mouseReleased() {
  Game.mouseState(mouseX, mouseY, mouseButton, false );
}