public class Ball extends GameObject {

  private boolean dragging, drawCue;
  private int friction = 900;
  private color c;

  public Ball(float x, float y, int radius, int mass, color c) {
    super(x, y, radius*2, radius*2, ID.Ball);
    this.c = c;
    this.mass = mass;
    this.radius = radius;
  }

  public void tick(GameObject other) {

    if (other != null)
    {
      Game.hit.play();
    }

    if (dragging) {
      y = Game.mousePosition.y;
      x = Game.mousePosition.x;
    } 

    vx += ax;
    vy += ay;
    x += vx;
    y += vy;
    ax = -vx /friction;
    ay = -vy /friction;

    if ((vx*vx + vy*vy) <= .001)
      vx = vy = 0;
  }

  public void render() {
    fill(c);
    noStroke();
    ellipse(x, y, w, h);

   /* if (vx != 0 || vy != 0) {
      stroke(255, 0, 0);
      line(x, y, vx*radius+x, vy*radius+y);
    }*/

    if (x-radius < 100)
    {
      vx = -vx;
      float dif = abs(100 - x+radius);
      x += (dif > 100)? 0 : dif;
    }

    if (x+radius > width - 100) {
      vx = -vx;
      float dif = abs(width - 100 - x-radius);
      x -= (dif > 100)? 0 : dif;
    }

    if (y-radius < 100)
    {
      vy = -vy;
      float dif = abs(100 - y+radius);
      y += (dif > 100)? 0 : dif;
    }

    if (y+radius > height - 100) {
      vy = -vy;
      float dif = abs(height - 100 - y-radius);
      y -= (dif > 100)? 0 : dif;
    }

    if (drawCue) {
      stroke(255, 0, 255);
      float d = dist(x, y, Game.mousePosition.x, Game.mousePosition.y);
      float sx = (x - Game.mousePosition.x) / d;
      float sy = (y - Game.mousePosition.y) / d;

      line(x, y, Game.mousePosition.x, Game.mousePosition.y);
      stroke(0, 0, 255);
      line(x, y, x+sx*1000, y+sy*1000);
    }
    stroke(0);
  }

  public void rightClickDown() {
    drawCue = true;
  }

  public void rightClickUp() {
    float d = dist(x, y, Game.mousePosition.x, Game.mousePosition.y);
    if (d > 200) {
      ax = (x - Game.mousePosition.x) / d * 2;
      ay = (y - Game.mousePosition.y) / d * 2;
    } else {
      ax = (x - Game.mousePosition.x) / d * (d / 100);
      ay = (y - Game.mousePosition.y) / d * (d / 100);
    }
    drawCue = false;
    Game.cue.play();
  }

  public void leftClickDown() {
    vx = vy = 0;
    ax = ay = 0;    
    dragging = true;
  }

  public void leftClickUp() {
    dragging = false;
  }

  public boolean intersects(GameObject other) {
    return intersectsEllipse(other);
  }

  public boolean contains(PVector point) {
    return containsEllipse(point);
  }
}