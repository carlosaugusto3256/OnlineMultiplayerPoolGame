import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.geom.Ellipse2D;
 
abstract class GameObject {

  public float x, y, vx, vy, w, h;
  public float ax, ay, radius, mass;
  public boolean rightClicked, leftClicked;
  protected ID id;

  public GameObject(float x, float y, float w, float h, ID id)
  {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;    
    this.id = id;
  }

  public abstract void tick(GameObject other);
  public abstract void render();
  public abstract boolean contains(PVector point);
  public abstract boolean intersects(GameObject other);
  public abstract void rightClickDown();
  public abstract void leftClickDown();
  public abstract void rightClickUp();
  public abstract void leftClickUp();

  protected boolean containsEllipse(PVector point) {
    return dist(point.x, point.y, x, y) <= radius;
  }

  protected boolean containsRectangle(PVector point) {
    return getRectangle().contains(point.x, point.y);
  }

  public boolean intersectsRectangle(GameObject other) {
    return getRectangle().intersects(other.getRectangle());
  }

  public boolean intersectsEllipse(GameObject other) {
    float d = dist(x, y, other.x, other.y);
    float radial = radius + other.radius;

    if (d < radial) {
      float overlap = (d - radius - other.radius) * 0.5;

      other.x += overlap * (x - other.x) / d;
      other.y += overlap * (y - other.y) / d;

      x -= overlap * (x - other.x) / d;
      y -= overlap * (y - other.y) / d;

      float nx = (other.x - x) / d;
      float ny = (other.y - y) / d;

      float kx = (vx - other.vx);
      float ky = (vy - other.vy);
      float p = 2.0 * (nx * kx + ny * ky) / (mass + other.mass);
      vx = vx - p * other.mass * nx;
      vy = vy - p * other.mass * ny;
      other.vx = other.vx + p * mass * nx;
      other.vy = other.vy + p * mass * ny;

      return true;
    }

    return false;
  }

  private Rectangle getRectangle() {
    return new Rectangle((int) x, (int)y, (int)w, (int)h);
  }

  public ID getId() {
    return id;
  }

  public void setId(ID id) {
    this.id = id;
  }
}